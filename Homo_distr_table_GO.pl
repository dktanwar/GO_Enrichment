#!/usr/bin/perl

use strict;
use warnings;

my $file = shift;
open(my $fh, "gunzip -c $file |") || die "Could not open the file $file\n";

my $sample_count;
my %sample;

while(my $line = <$fh>){
    chomp $line;
    next if $line =~ /^\#/;
    my @split = split(/\t/, $line);
    next unless $split[0]  == 9606;
    $sample_count->{$split[1]}->{$split[2]} = 1;
    $sample{$split[2]} = 1;
}
close ($fh);

my @sample_list = sort keys %sample;
print join("\t", "#Genes", @sample_list), "\n";
foreach my $gene (sort keys %{$sample_count}){
    my @line;
    push @line, $gene;
    foreach my $data (@sample_list) {
	if (exists $sample_count->{$gene}->{$data}) {
	    push @line, $sample_count->{$gene}->{$data};
	}else{
	    push @line, 0;
	}
    }
    print join("\t", @line), "\n";
}
