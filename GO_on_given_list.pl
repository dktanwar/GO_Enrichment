#!/usr/bin/perl

use strict;
use warnings;

my $file = shift;

print join( "\t", "GO_ID", "Counts_data", "GO_Term" ), "\n";

open( my $fh, "gunzip -c $file |" ) || die "Couldn't open the file $file\n";

my %hash;
my %function;

while ( my $line = <$fh> ) {
    chomp $line;
    next if $line =~ /^\#/;
    my @split = split( /\t/, $line );
    next unless $split[0] == 9606;
    my $j = $split[1];

    if ( exists $hash{$j} ) {
        my @now = @{ $hash{$j} };
        push @now, $split[2];
        $hash{$j} = \@now;
    }
    else {
        $hash{$j} = [ $split[2] ];
    }
    $function{ $split[2] } = $split[5];
}
close($fh);

my @genelist;
while ( my $l =  <STDIN> ) {
    chomp $l;
    next if $l =~ /^\#/;
#    print STDERR $l, "\n";
   push @genelist, $l;
}

my %gocount;

foreach my $g (@genelist) {
    if (exists $hash{$g}) {
	my @val = @{$hash{$g}};
	foreach my $v (@val) {
	    $gocount{$v}++;
	}
    }
}

foreach my $g (sort {$gocount{$b} <=> $gocount{$a}} keys %gocount) {
    my @line;
    push @line, $g, $gocount{$g};
    if (exists $function{$g}) {
	push @line, $function{$g};
    }else {
	push @line, "NA";
    }
    print join("\t", @line) , "\n";
}
